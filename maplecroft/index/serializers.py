"""Serialisers for the Index app."""

from rest_framework import serializers
from .models import Index


class IndexSerializer(serializers.ModelSerializer):
    """A simple view of each index."""

    class Meta:
        model = Index
        fields = ["id", "name"]


class StatSerializer(serializers.ModelSerializer):
    """The index but with extra fields for aggregate data."""

    max = serializers.FloatField()
    min = serializers.FloatField()
    mean = serializers.FloatField()
    median = serializers.FloatField()

    class Meta:
        model = Index
        fields = ["id", "name", "mean", "max", "min", "median"]


class WindowedSerializer(serializers.ModelSerializer):
    """The index with a windowed average."""

    averaged_scores = serializers.FloatField()
    time_from = serializers.DateTimeField(required=False)
    time_to = serializers.DateTimeField(required=False)

    class Meta:
        model = Index
        fields = ["id", "name", "averaged_scores", "time_from", "time_to"]
