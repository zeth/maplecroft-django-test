"""Extra Aggregation classes.
"""

from django.db.models import Aggregate, FloatField


class Median(Aggregate):
    """Warning POSTGRES only.
    Sources:
    https://www.postgresql.org/docs/current/sql-expressions.html#SYNTAX-AGGREGATES
    https://stackoverflow.com/a/61938397

    P.S. You can do it in generically but it's slower to loop through the Queryset.
    One value would look like this:
    from statistics import median
    index = Index.objects.get(id=1)
    median(index.indexversion_set.values_list('score', flat=True))
    """

    function = "PERCENTILE_CONT"
    name = "median"
    output_field = FloatField()
    template = "%(function)s(0.5) WITHIN GROUP (ORDER BY %(expressions)s)"
