"""Tests for the index serialisers.
Simple tech exercise serialisers don't have any logic. So nothing to test.
"""

from django.test import TestCase
from django.forms.models import model_to_dict
from nose.tools import eq_, ok_
from .factories import IndexFactory
from ..serializers import IndexSerializer


class TestIndexSerializer(TestCase):
    def setUp(self) -> None:
        self.index_data = model_to_dict(IndexFactory.build())

    def test_serializer_with_empty_data(self) -> None:
        serializer = IndexSerializer(data=None)
        eq_(serializer.is_valid(), False)

    def test_serializer_with_valid_data(self) -> None:
        serializer = IndexSerializer(data=self.index_data)
        ok_(serializer.is_valid())
