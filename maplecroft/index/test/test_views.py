import json
from datetime import timezone, datetime

from django.urls import reverse

from rest_framework.test import APITestCase
from rest_framework import status

# from rest_framework.exceptions import ValidationError

from faker import Faker
import factory

from .factories import IndexFactory, IndexVersionFactory


fake = Faker()


NAMES = ("Wealth", "Weapons", "Weather")


class TestIndexViewTestCase(APITestCase):
    """
    Tests /index operations.
    """

    def setUp(self) -> None:
        self.url = reverse("index-list")
        self.index_data = factory.build(dict, FACTORY_CLASS=IndexFactory)
        self.first_index = IndexFactory(name="Weather")
        self.second_index = IndexFactory(name="Wealth")
        self.second_index = IndexFactory(name="Weapons")

    def test_get_index_request(self) -> None:
        """Show the list of indicies."""
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Make sure we have an index
        content = json.loads(response.content)
        self.assertTrue(content["results"][0]["id"], 1)

    def test_search_index_by_query_arg(self) -> None:
        """Search for an index by id."""
        response = self.client.get(self.url, {"search": self.first_index.id})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Make sure we have an index
        content = json.loads(response.content)
        self.assertEqual(content["count"], 1)
        self.assertTrue(content["results"][0]["name"], "Weather")

    def test_order_by_name(self) -> None:
        """Order the indicies by name."""
        response = self.client.get(self.url, {"ordering": "name"})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        names_from_api = tuple([res["name"] for res in content["results"]])
        self.assertEqual(NAMES, names_from_api)

    def test_get_index_by_url_param(self) -> None:
        response = self.client.get(reverse("index-detail", args=[self.first_index.id]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Make sure we have a single index, not a list
        content = json.loads(response.content)
        self.assertEqual(content["name"], "Weather")


HAPPY = {"name": "Hapiness", "mean": 7.0, "max": 8.0, "min": 6.0, "median": 7.0}

CHAOS = {"name": "Chaos", "mean": 5.0, "max": 9.0, "min": 2.0, "median": 4.0}


class TestStatViewSetTestCase(APITestCase):
    """
    Tests /stats operations.
    """

    def setUp(self) -> None:
        self.url = reverse("stats-list")
        self.first_index = IndexFactory(name="Hapiness")
        self.second_index = IndexFactory(name="Chaos")
        self.first_score = IndexVersionFactory(index=self.first_index, score=8)
        self.second_score = IndexVersionFactory(index=self.first_index, score=6)
        self.third_score = IndexVersionFactory(index=self.second_index, score=2)
        self.forth_score = IndexVersionFactory(index=self.second_index, score=4)
        self.forth_score = IndexVersionFactory(index=self.second_index, score=9)

    def test_get_stats_request(self) -> None:
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Make sure we have an index
        content = json.loads(response.content)
        first_stats = content["results"][0]
        second_stats = content["results"][1]
        del first_stats["id"]
        del second_stats["id"]
        self.assertEqual(first_stats, HAPPY)
        self.assertEqual(second_stats, CHAOS)


START = "2021-12-05T00:00:00.000Z"
END = "2021-12-07T00:00:00.000Z"
FIRST_IN_RANGE = datetime(2021, 12, 6, 1, 44, 4, tzinfo=timezone.utc)
SECOND_IN_RANGE = datetime(2021, 12, 6, 2, 16, 9, tzinfo=timezone.utc)
OUT_OF_RANGE = datetime(2021, 11, 4, 1, 21, 2, tzinfo=timezone.utc)


class TestWindowedViewSet(APITestCase):
    """Aggregate all values for the window,
    and provide a single mean score."""

    def setUp(self) -> None:
        self.url = reverse("windowed-list")
        self.index = IndexFactory(name="Fish Stocks")
        self.first_score = IndexVersionFactory(
            index=self.index, score=1, timestamp=FIRST_IN_RANGE
        )
        self.second_score = IndexVersionFactory(
            index=self.index, score=3, timestamp=SECOND_IN_RANGE
        )
        self.third_score = IndexVersionFactory(
            index=self.index, score=9, timestamp=OUT_OF_RANGE
        )

    def test_get_window(self) -> None:
        response = self.client.get(self.url, {"time_from": START, "time_end": END})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(content["results"][0]["averaged_scores"], 2)

    def test_get_default_window(self) -> None:
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
