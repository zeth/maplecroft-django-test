from datetime import timezone

import factory


class IndexFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "index.Index"

    name = factory.Faker("name")


class IndexVersionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "index.IndexVersion"

    index = factory.SubFactory(IndexFactory)
    version = 1
    score = factory.Faker("random_digit_not_null")
    timestamp = factory.Faker("iso8601", tzinfo=timezone.utc)
