"""Views for the Index app for Maplecroft technical test.
Zeth 2021
"""

from datetime import datetime, timedelta, timezone

# The standard library's ISO 8601 parsing is terrible (long story),
# this one is better:
from dateutil.parser import isoparse

from django.db.models import Q, Avg, Max, Min, Value
from django.db.models.fields import DateTimeField

from rest_framework import viewsets, filters
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.exceptions import ValidationError

from .models import Index
from .serializers import IndexSerializer, StatSerializer, WindowedSerializer
from .extra_aggregation import Median
from django.db.models.query import QuerySet


class IndexViewSet(viewsets.ModelViewSet):
    """
    View available Indexes.

    Slightly bigger response than the spec to give a space for metadata,
    and there's a CSRF (Cross Site Request Forgery)
    attack based on Array redefinition.
    The best practice is to avoid top-level JSON arrays in API responses.
    """

    queryset = Index.objects.all()
    serializer_class = IndexSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    filter_backends = [filters.OrderingFilter, filters.SearchFilter]
    ordering_fields = ["name"]
    search_fields = ["name", "id"]


class StatViewSet(viewsets.ModelViewSet):
    """
    A high-level view of the Indexes: Mean Average, Min, Max and Median.
    """
    serializer_class = StatSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    filter_backends = [filters.OrderingFilter, filters.SearchFilter]
    ordering_fields = ["name"]
    search_fields = ["name", "id"]

    def get_queryset(self) -> QuerySet[Index]:
        """Add the additional values to the queryset."""
        return (
            Index.objects.annotate(
                mean=Avg("indexversion__score"),
                min=Min("indexversion__score"),
                max=Max("indexversion__score"),
                median=Median("indexversion__score"))
        )


class WindowedViewSet(viewsets.ModelViewSet):
    """
    Aggregate all values for the preceding 24 hours,
    and provide a single mean score.

    If this wasn't a technical test, I would go back to the customer
    and ask about why they want a list, and if I understood what it means,
    before spending time making a custom field.
    """

    serializer_class = WindowedSerializer
    queryset = Index.objects.all()
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_datetime_to(self) -> datetime:
        """The end of the time window."""
        try:
            time_to = self.request.query_params["time_to"]
        except KeyError:
            return datetime.now(tz=timezone.utc)

        try:
            return isoparse(time_to)
        except ValueError:
            raise ValidationError("The argument time_to could not be parsed.")

    def get_datetime_from(self, datetime_to: datetime) -> datetime:
        """The start of the time window."""
        try:
            time_from = self.request.query_params["time_from"]
        except KeyError:
            return datetime_to - timedelta(days=1)

        try:
            return isoparse(time_from)

        except ValueError:
            raise ValidationError("The argument time_from could not be parsed.")

    def get_queryset(self) -> QuerySet[Index]:
        """Set some sensible defaults.
        In reality, we would abstract out higher level handlers,
        e.g. have a robust way of dealing with datetimes in any endpoint.
        The timestamp in the spec is ambiguous so let's give both.
        """
        datetime_to = self.get_datetime_to()
        datetime_from = self.get_datetime_from(datetime_to)

        range = [datetime_from, datetime_to]

        averaged_scores = Avg(
            "indexversion__score", filter=Q(indexversion__timestamp__range=range)
        )

        return (
            Index.objects.annotate(
                averaged_scores=averaged_scores,
                time_from=Value(datetime_from, output_field=DateTimeField()),
                time_to=Value(datetime_to, output_field=DateTimeField())
        ))
