from django.conf import settings
from django.urls import path, re_path, include, reverse_lazy
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic.base import RedirectView
from django.shortcuts import redirect


from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views
from .users.views import UserViewSet, UserCreateViewSet
from .index.views import IndexViewSet, StatViewSet, WindowedViewSet

router = DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'users', UserCreateViewSet)
router.register(r'windowed', WindowedViewSet, basename='windowed')
router.register(r'stats', StatViewSet, basename='stats')
router.register(r'index', IndexViewSet, basename='index')


def redirect_windowed(request, index_id):
    """Give a second url to the windowed view.
    We can do this as in the spec:
    /{index_id}/windowed?time_from=<iso_string>&time_to=<iso_string>
    
    But I would also suggest to the customer that the api has a resource keyword,
    that is also more consistent with the other endpoints:
    
    /windowed/{index_id}/?time_from=<iso_string>&time_to=<iso_string>
    """
    return redirect(
        f"/api/v1/windowed/{index_id}/?{request.META['QUERY_STRING']}",
        permanent=False)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/<int:index_id>/windowed/', redirect_windowed),
    path('api/v1/', include(router.urls)),
    path('api-token-auth/', views.obtain_auth_token),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    # the 'api-root' from django rest-frameworks default router
    # http://www.django-rest-framework.org/api-guide/routers/#defaultrouter
    re_path(r'^$', RedirectView.as_view(url=reverse_lazy('api-root'), permanent=False)),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
